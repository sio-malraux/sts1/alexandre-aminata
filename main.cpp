#include <iostream>
#include <iomanip> /*<iomanip> contient les manipulateurs pour les flux standards
 la plupart sert à formatter les données (aligner à gauche, à droite,
 remplir les espaces par un certain caractère, fixer la précision des nombres à virgules*/
#include <limits>
#include <cstring>
#include <libpq-fe.h>

using namespace std;
int main()
{
  const char *connexion_infos;
  PGconn *connexion;
connexion_infos = "host='postgresql.bts-malraux72.net' port=5432 dbname='a.marques' user='a.marques' password=P@ssword connect_timeout=5";
  unsigned short int nb_caracteres;

  if(PQping(connexion_infos) == PQPING_OK)
  {
    // Établissement de la connexion avec les paramètres
    // fournis dans la chaîne de caractères 'connexion_infos'
    connexion = PQconnectdb(connexion_infos);

    if(PQstatus(connexion) == CONNECTION_OK)
    {
      cout << "La connexion au serveur de base de données '" << PQhost(connexion)  << "' a été établie avec les paramètres suivants :" << endl;
      cout << " * utilisateur : " << PQuser(connexion) << endl;
      cout << " * mot de passe : ";
      nb_caracteres = sizeof PQpass(connexion);

      for(unsigned int i = 0; i < nb_caracteres; ++i)
      {
        cout << '*';
      }

      cout << " la longueur du mot de passe" << endl;
      cout << " * base de données : " << PQdb(connexion) << endl;
      cout << " * port TCP : " << PQport(connexion) << endl;
      cout << boolalpha << " * chiffrement SSL : " << (bool)(PQgetssl(connexion) != 0) << endl; //boolalpha Ecrit/lit les données de type bool sous forme textuelle, càd true ou false.
      cout << " * encodage : " << pg_encoding_to_char(PQclientEncoding(connexion)) << endl;
      cout << " * version du protocole : " << PQprotocolVersion(connexion) << endl;
      cout << " * version du serveur : " << PQserverVersion(connexion) << endl;
      cout << " * version de la bibliothèque 'libpq' du client : " << PQlibVersion() << endl;
      // Récupération des données
      const char *requete_sql = "SELECT si6.\"Animal\".id, si6.\"Animal\".nom as \"nom de l\'animal\", sexe, date_naissance AS \"date de naissance\", commentaires, si6.\"Race\".nom AS race, description FROM si6.\"Animal\" JOIN si6.\"Race\" on si6.\"Animal\".race_id = si6.\"Race\".id WHERE si6.\"Animal\".sexe = 'Femelle' AND si6.\"Race\".nom = 'Singapura';";
      PGresult *resultats = PQexec(connexion, requete_sql);
      ExecStatusType etat_execution = PQresultStatus(resultats);

      // Vérification de l'état des données reçues
      if(etat_execution == PGRES_TUPLES_OK)
      {
        // Exploitation des résultats
        const char separateur = '|';
        const char marge = ' ';
        const char ligne = '-';
        unsigned int taille_separateur = 1;
        unsigned int taille_marge = 1;
        unsigned int taille_champ = 0;
        unsigned int taille_ligne = 0;
        char *valeur_champ;
        char *nom_champ;
        unsigned int champ_max = numeric_limits<unsigned int>::min(); // ignorer le maximum de caractères différents grace a numeric_limits

        // Recherche de la taille du nom de champ le plus large
        for(unsigned int compteur_champ = 0; (int)compteur_champ < PQnfields(resultats); ++compteur_champ)
        {
          nom_champ = PQfname(resultats, compteur_champ);
          taille_champ = strlen(nom_champ);

          if(champ_max < taille_champ)
          {
            champ_max = taille_champ;
          }
        }

        taille_ligne = PQnfields(resultats) * (taille_separateur + taille_marge + champ_max + taille_marge) + taille_separateur;
        cout << left;  // Alignement des champs à gauche

        // Affichage de l'en-tête

	for(unsigned int numero_caractere = 0; numero_caractere < taille_ligne; ++numero_caractere)
        {
          cout << ligne;
        }

	//ligne separatrice
        for(unsigned int compteur_champ = 0; (int)compteur_champ < PQnfields(resultats); ++compteur_champ)
        {
          nom_champ = PQfname(resultats, compteur_champ);
          cout << separateur << marge << setw(champ_max) << nom_champ << marge;
        }

        cout << separateur << endl;

        for(unsigned int numero_caractere = 0; numero_caractere < taille_ligne; ++numero_caractere)
        {
          cout << ligne;
        }

        cout << endl;

        // Affichage des tuples
        for(unsigned int compteur_tuple = 0; (int)compteur_tuple < PQntuples(resultats); ++compteur_tuple)
        {
          for(unsigned int compteur_champ = 0; (int)compteur_champ < PQnfields(resultats); ++compteur_champ)
          {
            char *trc;
            trc = (char *)malloc(champ_max * sizeof(char) + 1);//malloc permet d'allouer dynamiquement de la mémoire.
            valeur_champ = PQgetvalue(resultats, compteur_tuple, compteur_champ);

	   if(PQgetlength(resultats, compteur_tuple, compteur_champ) > (int)champ_max)
            {
              valeur_champ = strncpy(trc, valeur_champ, champ_max);//strncpy copie une chaine
              valeur_champ[champ_max] = 0;

              for(unsigned int i = 1; i <= 3; valeur_champ[champ_max - i++] = '.');
            }

            cout << separateur << marge << setw(champ_max) << valeur_champ << marge; //setw decale en reference de champ_max la valeur de valeur_champ
            free(trc);//libération de la mémoire
          }

          cout << separateur << endl;
        }

        cout << endl;

        for(unsigned int numero_caractere = 0; numero_caractere < taille_ligne; ++numero_caractere)
        {
          cout << ligne;
        }

	cout << "L'exécution de la requête SQL a retourné " << PQntuples(resultats) << " enregistrement" << (PQntuples(resultats) > 1 ? 's' : '\0') << "." << endl;


      }
      else if(etat_execution == PGRES_EMPTY_QUERY)
      {
        cerr << "La chaîne envoyée au serveur était vide." << endl;
      }
      else if(etat_execution == PGRES_COMMAND_OK)
      {
        cerr << "La requête SQL n'a renvoyé aucune donnée." << endl;
      }
      else if(etat_execution == PGRES_COPY_OUT)
      {
        cerr << "Début de l'envoi (à partir du serveur) d'un flux de données." << endl;
      }
      else if(etat_execution == PGRES_COPY_IN)
      {
        cerr << "Début de la réception (sur le serveur) d'un flux de données." << endl;
      }
      else if(etat_execution == PGRES_BAD_RESPONSE)
      {
        cerr << "La réponse du serveur n'a pas été comprise." << endl;
      }
      else if(etat_execution == PGRES_NONFATAL_ERROR)
      {
        cerr << "Une erreur non fatale (une note ou un avertissement) est survenue." << endl;
      }
      else if(etat_execution == PGRES_FATAL_ERROR)
      {
        cerr << "Une erreur fatale est survenue." << endl;
      }
      else if(etat_execution == PGRES_COPY_BOTH)
      {
        cerr << "Lancement du transfert de données Copy In/Out (vers et à partir du serveur)." << endl;
      }
      else if(etat_execution == PGRES_SINGLE_TUPLE)
      {
        cerr << "La structure PGresult contient une seule ligne de résultat provenant de la commande courante." << endl;
      }

      PQclear(resultats);
    }
    else
    {
      cerr << "Malheureusement la connexion n'a pas pu être établie" << endl;
    }

    PQfinish(connexion);
  }
  else
  {
    cerr << "Malheureusement le serveur n'est pas joignable. Vérifier la connectivité" << endl;
  }

  return 0;
}


